#include <atomic>
#include <iostream>

class Control
{
private:
    std::atomic_uint counter;
public:
    Control()
    {
        counter=0;
    }
    void increase()
    {
        counter++;
    }
    void decrease()
    {
        counter--;
    }
    size_t size()
    {
        return static_cast<size_t> (counter);
    }
};

template <typename T>
class SharedPtr
{
private:
    Control* counter;
    T* data;
public:
    SharedPtr()
    {
        data=NULL;
        counter=NULL;
    }
    SharedPtr(T* value)
    {
        data=value;
        counter=new Control();
        counter->increase();
    }
    SharedPtr(const SharedPtr& value)
    {
        counter=value.counter;
        data=value.data;
        if(counter!=NULL)
        {
            counter->increase();
        }
    }
    SharedPtr(SharedPtr&& value)
    {
        counter=value.counter;
        data=value.data;
        if(counter!=NULL)
        {
            counter->increase();
        }
        value.counter = NULL;
        value.data = NULL;
    }
    ~SharedPtr()
    {
        if((data==NULL)||(counter==NULL))
            return;
        else
        {
            counter->decrease();
            if(counter->size()==0)
            {
                delete data;
                delete counter;
            }
        }
    }
    auto operator=(const SharedPtr& value)->SharedPtr&
    {
        if(data!=NULL)
        {
            counter->decrease();
            if(counter->size()==0)
            {
                delete counter;
                delete data;
            }
        }
        counter=value.counter;
        if(counter!=NULL) 
            counter->increase();
        data=value.data;
        return (*this);
    }

    auto operator=(SharedPtr&& value)->SharedPtr&
    {
        if(data!=NULL)
        {
            counter->decrease();
            if(counter->size()==0)
            {
                delete counter;
                delete data;
            }
        }
        counter=value.counter;
        data=value.data;
        if(value.data!=NULL)
        {
            delete value.counter;
            delete value.data;
        }
        return (*this);
    }
    operator bool() const
    {
        if(data==NULL)
            return false;
        if(counter->size()==0)
            return false;
        return true;
    }
    auto operator*() const->T&
    {
        return *data;
    }
    auto operator->() const->T*
    {
        return data;
    }
    auto get()->T*
    {
        return data;
    }
    void reset() 
    {
        if(counter!=NULL)
        {
            counter->decrease();
            if(counter->size()==0)
            {
                delete data;
                delete counter;
            }
        }
        data=NULL;
        counter=NULL;
    }
    void reset(T* ptr)
    {
        reset();
        data=ptr;
        counter=new Control();
        counter->increase();
    }
    void swap(SharedPtr& value)
    {
        auto buf1=value.data;
        value.data=data;
        data=buf1;
        auto buf2=value.counter;
        value.counter=counter;
        counter=buf2;
    }
    auto use_count() const->size_t
    {
        if(!*this)
            return 0;
        return counter->size();
    }
};

int main() 
{
    int* p=new int;
    SharedPtr<int> ptr1(p);
    SharedPtr<int> ptr2(ptr1);
    SharedPtr<int> ptr3(ptr2);
    SharedPtr<int> ptr4(ptr3);
    std::cout << ptr1.use_count() << std::endl;
    ptr3.reset();
    ptr4.reset();
    std::cout << ptr3.use_count() << std::endl;
    SharedPtr<int> ptr5(ptr4);
    std::cout << ptr1.use_count() << std::endl;
    std::cout << ptr5.use_count() << std::endl;
    std::cout << ptr2.use_count() << std::endl;
    system("pause");
    return 0;
}